import { splitContainer, toText } from '@inkylabs/remark-utils'
import Cite from 'citation-js'
import fraktur from 'fraktur'
import { promises as fs } from 'fs'
import glob from 'globby'
import mime from 'mime-types'
import path from 'path'

const NAME = 'pulpweaver-html'

const VOID_ELEMENTS = new Set([
  'area',
  'base',
  'br',
  'col',
  'embed',
  'hr',
  'img',
  'input',
  'link',
  'meta',
  'param',
  'source',
  'track',
  'wbr'
])

function padNum (n) {
  return String(n).padStart(4, '0')
}

function blockSpec (tag, attrs) {
  attrs = Object.assign({}, attrs)
  if (attrs.class) {
    if (attrs.class.length) {
      attrs.class = attrs.class.join(' ')
    } else {
      delete attrs.class
    }
  }
  if (attrs['-']) delete attrs['-']
  const sattrs = Object.entries(attrs)
    .map(([k, v]) => `${k}="${v}"`)
    .join(' ')
  const space = sattrs ? ' ' : ''
  return `${tag}${space}${sattrs}`
}

function blockEnds (tag, attrs) {
  return {
    begin: `<${blockSpec(tag, attrs)}>`,
    end: `</${tag}>`
  }
}

function closedBlock (tag, attrs, ctx) {
  const spec = blockSpec(tag, attrs)
  ctx.output += VOID_ELEMENTS.has(tag) || ctx.xhtml
    ? `<${spec}/>`
    : `<${spec}/></${tag}>`
}

function wrapBlock (node, ctx, tag, attrs) {
  attrs = Object.assign({}, node.attributes || {}, attrs || {})
  const b = blockEnds(tag, attrs)
  ctx.output += b.begin
  handleBlock(node, ctx)
  ctx.output += b.end
}

function handleNode (node, ctx) {
  if (!ctx.usedsources) ctx.usedsources = {}
  if (!ctx.pagemarks) {
    ctx.pagemarks = []
    ctx.allowPagemark = true
  }
  if (!ctx.pages) ctx.pages = []
  if (!ctx.footnotes) ctx.footnotes = []
  if (!ctx.images) ctx.images = []
  if (!ctx.indexEntries) ctx.indexEntries = []
  if (!ctx.figures) ctx.figures = []
  if (!ctx.labels) ctx.labels = []
  if (!ctx.output) ctx.output = ''

  if (ctx.allowPagemark) {
    ctx.pagemarks.forEach(pm => handlePagemark(pm, ctx))
    ctx.pagemarks = []
  }

  switch (node.type) {
    case 'attribution':
      handleAttribution(node, ctx)
      break
    case 'blockquote':
      handleBlockquote(node, ctx)
      break
    case 'caption':
      // This is handled by handleFigure.
      break
    case 'Cite':
    case 'cite':
      handleCite(node, ctx)
      break
    case 'comment':
      break
    case 'controlspace':
      ctx.output += ' '
      break
    case 'definition':
      handleDefinition(node, ctx)
      break
    case 'emphasis':
      wrapBlock(node, ctx, 'em')
      break
    case 'figure':
      handleFigure(node, ctx)
      break
    case 'fraktur':
      ctx.output += fraktur.encode(toText(node, ctx.file))
      break
    case 'footnote':
      handleFootnote(node, ctx)
      break
    case 'hang':
      handleBlock(node, ctx)
      break
    case 'heading':
      handleHeading(node, ctx)
      break
    case 'html':
      if (!node.value.startsWith('<!--')) ctx.output += node.value
      break
    case 'image':
      handleImage(node, ctx)
      break
    case 'indexfmt':
      handleIndexFmt(node, ctx)
      break
    case 'indexstart':
    case 'indexend':
      handleIndex(node, ctx)
      break
    case 'label':
      handleLabel(node, ctx)
      break
    case 'linebreak':
      ctx.output += '<br />'
      break
    case 'link':
      wrapBlock(node, ctx, 'a', {
        href: node.url
      })
      break
    case 'list':
      handleList(node, ctx)
      break
    case 'listItem':
      handleListItem(node, ctx)
      break
    case 'medspace':
      ctx.output += '     '
      break
    case 'noindent':
      break
    case 'pagemark':
      if (ctx.allowPagemark) {
        handlePagemark(node, ctx)
      } else {
        ctx.pagemarks.push(node)
      }
      break
    case 'paragraph':
      handleParagraph(node, ctx)
      break
    case 'ref':
      handleRef(node, ctx)
      break
    case 'root':
      handleBlock(node, ctx)
      break
    case 'strong':
      wrapBlock(node, ctx, 'b')
      break
    case 'smallcaps':
      wrapBlock(node, ctx, 'span', {
        class: ['sc']
      })
      break
    case 'softhyphen':
      ctx.output += '\u00AD'
      break
    case 'span':
      wrapBlock(node, ctx, 'span')
      break
    case 'subheading':
      handleSubheading(node, ctx)
      break
    case 'subscript':
      wrapBlock(node, ctx, 'sub')
      break
    case 'superscript':
      wrapBlock(node, ctx, 'sup')
      break
    case 'table':
      handleTable(node, ctx)
      break
    case 'tableBody':
      wrapBlock(node, ctx, 'tbody')
      break
    case 'tableCell':
      handleTableCell(node, ctx)
      break
    case 'tableHead':
      wrapBlock(node, ctx, 'thead')
      break
    case 'tableRow':
      handleTableRow(node, ctx)
      break
    case 'tex':
      break
    case 'text':
      handleText(node, ctx)
      break
    case 'underline':
      wrapBlock(node, ctx, 'u')
      break
    default:
      ctx.file.message(`unknown type: ${node.type}`, node.position,
        `${NAME}:no-unknown-type`)
  }
}

function handleBlock (node, ctx) {
  ctx.blockStart = true
  for (const c of node.children) {
    handleNode(c, ctx)
    ctx.blockStart = false
  }
  ctx.blockStart = false
}

function handleAttribution (node, ctx) {
  ctx.output += '<br/>\n'
  ctx.output += '—'
  wrapBlock(node.labelNode.children[0], ctx, 'b')
  if (node.contentsNode.children.length) {
    ctx.output += ',\n'
    handleBlock(node.contentsNode.children[0], ctx)
  }
  ctx.output += '\n'
}

function handleBlockquote (node, ctx) {
  const fw = node.attributes.class.includes('fullwidth')
  const npb = node.attributes.class.includes('nopagebreak')
  if (!ctx.blockStart) ctx.output += '\n\n'
  const div = blockEnds('div', node.attributes)
  if (npb) ctx.output += div.begin
  if (fw) {
    ctx.output += '<p>'
    ctx.output += '“'
    handleBlock(node.children[0], ctx)
    ctx.output += '”'
    node.children.slice(1).forEach(c => handleNode(c, ctx))
    ctx.output += '</p>'
  } else {
    wrapBlock(node, ctx, 'blockquote', node.attributes)
  }
  if (npb) ctx.output += div.end
}

function handleBlockText (node, ctx) {
  const sctx = Object.assign({}, ctx, { output: '' })
  handleBlock(node, sctx)
  return sctx.output
}

function handleCite (node, ctx) {
  const key = toText(node)
  const source = ctx.sources[key]
  if (!source) {
    ctx.file.message(`unknown source ${key}`, node.position,
      `${NAME}:known-sources`)
    return
  }
  const cite = new Cite()
  const c = cite.set(source).get({
    format: 'string',
    type: 'html',
    style: 'citation-apa'
  })
    .replace(/^<div.*?<div.*?>(.*)<\/div>.*?div>$/s, '$1')
    .replace(/\(n.d.\)\. /g, '') // citation-js adds this if there is no date.
    .replace(/\.$/, '') // We'll add a final period in the text if we want it.
  ctx.output += c
  ctx.usedsources[key] = c

  const pages = node.attributes.p
  if (pages) {
    const bibre = new RegExp(key + '.*?^}$', 'ms')
    const m = source._graph[0].data.match(bibre)
    if (!m) {
      ctx.file.message(`could not parse "${key}" from .bib file`,
        node.position, `${NAME}:parseable-bib`)
      return
    }
    const m2 = m[0].match(/pagination=\{(.*?)\}/)
    const pagination = m2 ? m2[1] : 'page'

    const multi = !!pages.match(/[-–,]/)
    let prefix
    switch (pagination) {
      case 'page':
        prefix = multi ? 'pp.' : 'p.'
        break
      case 'section':
        prefix = multi ? '§§' : '§'
        break
      default:
        ctx.file.message(`unknown pagination "${pagination}" for "${key}"`,
          node.position, `${NAME}:known-pagination`)
    }
    ctx.output += ` ${prefix} ${pages}`
  }
}

function handleDefinition (node, ctx) {
  const dl = blockEnds('dl')
  ctx.output += dl.begin
  const [label, content] = splitContainer(node, ctx.file)
  wrapBlock(label, ctx, 'dt')
  wrapBlock({ children: content }, ctx, 'dd')
  ctx.output += dl.end
}

function handleFigure (node, ctx) {
  const captions = node.children.filter(c => c.type === 'caption')
  const index = ctx.figures.length + 1
  const id = `fg${ctx.chapterlabel}_${index}`
  const attrs = { class: ['figure'] }
  if (captions.length) attrs.id = id
  const f = blockEnds('div', attrs)
  ctx.output += f.begin
  handleBlock(node, ctx)

  if (captions.length) {
    const index = ctx.figures.length + 1
    const id = `fg${ctx.chapterlabel}_${index}`
    const p = blockEnds('p', {
      class: ['caption']
    })
    ctx.output += p.begin
    const c = ctx.chapterlabel || ''
    if (!c) {
      ctx.file.message('No chapter label set for figure', node.position,
        `${NAME}:figure-chapter`)
    }
    ctx.output += `Figure ${c}.${index}: `
    handleBlock(captions[0], ctx)
    ctx.output += p.end

    const sctx = { output: '' }
    wrapBlock(captions[0], sctx, 'span')

    ctx.figures.push({
      caption: sctx.output,
      index,
      id
    })
  }
  ctx.output += f.end
}

// http://kb.daisy.org/publishing/docs/html/notes.html
function handleFootnote (node, ctx) {
  const num = ctx.footnotes.length + 1
  const clabel = ctx.chapterlabel || ctx.title.toLowerCase()
  const id = `${clabel}_${num}`
  ctx.footnotes.push({
    id,
    num,
    content: handleBlockText(node, ctx)
  })

  const sup = blockEnds('sup', {
    class: ['footnotemark']
  })
  const link = blockEnds('a', {
    id: `#fm${id}`,
    href: `#fn${id}`,
    role: 'doc-noteref',
    'epub:type': 'noteref'
  })
  // The wordjoiner ensures the footnote mark doesn't show up on a new line.
  ctx.output += '&#8288;'
  ctx.output += sup.begin
  ctx.output += link.begin
  ctx.output += num
  ctx.output += link.end
  ctx.output += sup.end
}

function handleHeading (node, ctx) {
  if (!node.attributes['-']) {
    switch (ctx.type) {
      case 'appendix':
        ctx.appendixnum++
        ctx.chapterlabel = String.fromCharCode(ctx.appendixnum)
        break
      case 'chapter':
        ctx.chapternum++
        ctx.chapterlabel = String(ctx.chapternum)
        break
      default:
        ctx.file.message(`headings not allowed for type "${ctx.type}"`,
          node.position, `${NAME}:heading-type`)
    }
  }
  switch (node.depth) {
    case 1:
      ctx.depth = 1
      ctx.title = handleBlockText(node, ctx)
      if (node.subheading) ctx.subtitle = handleBlockText(node.subheading, ctx)
      break
    case 2:
      ctx.depth = 2
      wrapBlock(node, ctx, 'h2')
      break
    case 3:
      ctx.depth = 3
      wrapBlock(node, ctx, 'h3')
      break
    case 4:
      ctx.depth = 4
      wrapBlock(node, ctx, 'h4')
      break
    default:
      ctx.file.message(`unhandled heading depth: ${node.depth}`, node.position,
        `${NAME}:known-heading-depth`)
  }
}

function handleImage (node, ctx) {
  const mimetype = mime.lookup(node.url)
  if (!mimetype) {
    ctx.file.message(`Unknown mimetype for ${node.url}`, node.position,
      `${NAME}:known-mimetypes`)
  }
  ctx.images.push({
    path: node.url,
    mimetype,
    id: padNum(ctx.chapterindex) + '_' + padNum(ctx.images.length)
  })
  const src = path.join(ctx.imagePrefix, node.url)
  closedBlock('img', Object.assign({
    src,
    alt: node.alt
  }, node.attributes), ctx)
}

function handleIndexFmt (node, ctx) {
}

function handleIndex (node, ctx) {
  const idx = ctx.idxs[node.indexKey]

  let entry
  let found = false
  for (let i = 0; i < idx.sequence.length; i++) {
    entry = idx.sequence[i]
    if (entry.gk === node.groupKey && entry.ek === node.entryKey) {
      idx.sequence.splice(i, 1)
      entry.id = `idx${node.indexKey}${entry.id}`
      found = true
      break
    }
  }
  if (!found) {
    const k = `${node.indexKey}.${node.groupKey}.${node.entryKey}`
    ctx.file.message(`index entry not found: "${k}"`, node.position,
      `${NAME}:index-entry-found`)
  }

  if (node.type !== 'indexstart') return

  ctx.indexEntries.push(entry.id)

  closedBlock('span', {
    id: entry.id
  }, ctx)
}

function handleLabel (node, ctx) {
  ctx.labels[node.value] = null
}

function handleList (node, ctx) {
  ctx.allowPagemark = false
  const attrs = Object.assign({}, node.attributes)
  if (node.start) attrs.start = node.start
  wrapBlock(node, ctx, node.ordered ? 'ol' : 'ul', attrs)
  ctx.allowPagemark = true
}

function handleListItem (node, ctx) {
  ctx.allowPagemark = true
  wrapBlock(node, ctx, 'li')
  ctx.allowPagemark = false
}

function handlePagemark (node, ctx) {
  ctx.pages.push(parseInt(node.page))
  ctx.pageLabel = node.label
  closedBlock('span', {
    role: 'doc-pagebreak',
    id: `pg${node.page}`,
    'aria-label': node.label
  }, ctx)
}

function handleParagraph (node, ctx) {
  const a = node.attributes
  if (a && a.class.includes('caption')) return
  wrapBlock(node, ctx, 'p')
}

function handleRef (node, ctx) {
  ctx.output += `${ctx.refPrefix}{${node.value}}`
}

function handleSubheading (node, ctx) {
}

function handleTable (node, ctx) {
  wrapBlock(node, ctx, 'table')
}

function handleTableCell (node, ctx) {
  wrapBlock(node, ctx, node.header ? 'th' : 'td', {
    style: `text-align: ${node.align}`
  })
}

function handleTableRow (node, ctx) {
  wrapBlock(node, ctx, 'tr')
}

function handleText (node, ctx) {
  ctx.output += node.value
    .replace(/&/g, '&amp;')
}

async function replaceLabels (globals) {
  const files = await glob('genfiles/**/*.json')
  const genfiles = []
  for (const f of files) {
    const text = await fs.readFile(f, 'utf8')
    genfiles.push({
      filepath: f,
      data: JSON.parse(text)
    })
  }

  const labels = {}
  for (const { data } of genfiles) {
    for (const k of data.labels) {
      labels[k] = data.chapterlabel
    }
  }
  const refre = new RegExp(globals.refPrefix + '\\{(.*?)\\}', 'g')
  for (const { filepath, data } of genfiles) {
    data.content = data.content.replace(refre, (_, k) => labels[k])
    data.footnotes.forEach(f => {
      f.content = f.content.replace(refre, (_, k) => labels[k])
    })
    await fs.writeFile(filepath, JSON.stringify(data, null, 2))
  }
}

export {
  handleNode,
  replaceLabels
}
